/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author JUNIOR RIZO
 */
public class Registro {
    String codigo;
    String fechaRegistro;
    Cliente personaDifunto;
    Instructor personaResponsable;
    Clase tierra;

    public Registro() {
    }

    public Registro(String codigo, String fechaRegistro, Cliente personaDifunto, Instructor personaResponsable, Clase tierra) {
        this.codigo = codigo;
        this.fechaRegistro = fechaRegistro;
        this.personaDifunto = personaDifunto;
        this.personaResponsable = personaResponsable;
        this.tierra = tierra;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Cliente getPersonaDifunto() {
        return personaDifunto;
    }

    public void setPersonaDifunto(Cliente personaDifunto) {
        this.personaDifunto = personaDifunto;
    }

    public Instructor getPersonaResponsable() {
        return personaResponsable;
    }

    public void setPersonaResponsable(Instructor personaResponsable) {
        this.personaResponsable = personaResponsable;
    }

    public Clase getTierra() {
        return tierra;
    }

    public void setTierra(Clase tierra) {
        this.tierra = tierra;
    }

    @Override
    public String toString() {
        return "Registro{" + "codigo=" + codigo + ", fechaRegistro=" + fechaRegistro + ", personaDifunto=" + personaDifunto + ", personaResponsable=" + personaResponsable + ", tierra=" + tierra + '}';
    }
    
    
}
