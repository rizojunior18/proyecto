/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author JUNIOR RIZO
 */
public class Instructor extends Persona {
    private String direccion;

    public Instructor() {
    }


    public Instructor(String nombres, String apellidos, String noCedula,String direccion) {
        super(nombres, apellidos, noCedula);
        this.direccion = direccion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    
    
}
